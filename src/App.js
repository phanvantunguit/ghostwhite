import './App.css';
import React from 'react';
import { BrowserRouter as Router, Routes, Link, Route } from 'react-router-dom';
import Home from './containers/Home';
import RAndD from './containers/R&D';
import Agency from './containers/Agency';
import GhostPolicy from './containers/GhostPolicy';
import GhostLegalPage from './containers/TheLegal';
import AboutContact from './containers/AboutContact';

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="a-and-d" element={<RAndD />} />
        <Route path="/about-us" element={<Agency />} />
        {/* <Route path="privacy-policy" element={<GhostPolicy />} /> */}
        <Route path="legal" element={<GhostLegalPage />} />
        <Route path="contact" element={<AboutContact />} />
      </Routes>
    </>
  );
}

export default App;
