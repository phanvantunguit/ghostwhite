import { ADMIN, GUEST, USER } from './permission';

export const publicRoutes = [
  // {
  //   path: '/login',
  //   component: 'LoginPage',
  //   exact: true,
  //   permission: GUEST
  // }
  {
    path: '/home',
    component: 'HomePage',
    exact: true,
    permission: GUEST,
  },
  {
    path: '/a-and-d',
    component: 'ADPage',
    exact: true,
    permission: GUEST,
  },
  {
    path: '/about-contact',
    component: 'AboutContact',
    exact: true,
    permission: GUEST,
  },
  {
    path: '/partner',
    component: 'GhostPartnerPage',
    exact: true,
    permission: GUEST,
  },
  {
    path: '/Legal',
    component: 'GhostLegalPage',
    exact: true,
    permission: GUEST,
  },
  // {
  //   path: "/privacy-policy",
  //   component: "GhostPolicyPage",
  //   exact: true,
  //   permission: GUEST,
  // },
];

export const privateRoutes = [
  // {
  //   path: '/home',
  //   component: 'HomePage',
  //   exact: true,
  //   permission: USER
  // },
  {
    path: '/setting',
    component: 'SettingPage',
    exact: true,
    permission: ADMIN,
  },
];
