import React, { useEffect } from "react";
import { Row, Col } from "antd";
import styles from "./styled.module.scss";
import ReactGA from "react-ga4";

const GhostPolicy = () => {
  useEffect(() => {
    ReactGA.send("pageview");
  }, []);
  return (
    <>
      {" "}
      <Row className={styles.rowContainer}>
        <Col xs={20} xl={16} className={styles.container}>
          <p>
            We prepared this privacy policy (Privacy Policy), which should be
            read together with our Terms of Service, to explain how GHOST uses
            and protects your personal information. We have made every effort to
            ensure clarity in our Privacy Policy.
          </p>
          <p>
            You can contact us at any time by email at legal@GoGhost.org for any
            inquiries, questions, comments regarding your data and personal
            information or this Privacy Policy.
          </p>
          <p>
            By using our Services, you confirm your acceptance of our Privacy
            Policy. If you do not agree to our Privacy Policy, you may not use
            our Services.
          </p>
          <h1>Privacy Policy</h1>
          <h3>Updated: Oct 25th, 2022</h3>
          <p>
            We are committed to limiting our collection of your information to
            what is necessary to provide you with our Services.
          </p>
          <p>
            This data is collected in order to provide our secure communication
            services (Services) or to allow you to interact with our website
            located at https://GoGhost.org/ (Website).
          </p>
          <h2>Data You Provide</h2>
          <h3>Data You Pro </h3>
          <p>
            We only see the information you give us. This would only include
            such things as required billing and contact information for
            purchasing our Services.
          </p>
          <p>
            Deletion is forever. When you delete a message, or when a message
            expires, our “secure shredder” technology uses forensic deletion
            techniques to ensure that your data can never be recovered by us or
            anyone else.
          </p>
          <p>
            You own your data. We do not share or sell any data about our users.
            Period.
          </p>
        </Col>
      </Row>
    </>
  );
};
export default GhostPolicy;
